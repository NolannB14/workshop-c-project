/*
** EPITECH PROJECT, 2021
** matchstick
** File description:
** Game structures definition for Matchstick project
*/


#ifndef GAMESTRUCT_H
#define GAMESTRUCT_H

#include "macros.h"

typedef struct {
    ret_t cur_player;
    short lines;
    short cols;
    short matches;
    char **map;
} game_t;

#endif /* GAMESTRUCT_H */
