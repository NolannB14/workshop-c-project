/*
** EPITECH PROJECT, 2020
** My_str_startwith
** File description:
** Check if a string is the start of the other
*/

int my_str_startwith(const char *to_find, const char *str)
{
    for (int i = 0; to_find[i]; i++)
        if (to_find[i] != str[i])
            return 0;
    return 1;
}
