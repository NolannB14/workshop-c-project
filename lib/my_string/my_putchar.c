/*
** EPITECH PROJECT, 2020
** My_putchar
** File description:
** Display one character
*/

#include <unistd.h>

void my_putchar(const char c)
{
    write(1, &c, 1);
}
